﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IJunior.TypedScenes;

public class PreviewLoader : MonoBehaviour
{
    public void LoadPreview()
    {
        Preview.Load();
    }
}
