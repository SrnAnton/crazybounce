using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using IJunior.TypedScenes;

public class TowerBuilder : MonoBehaviour, ISceneLoadHandler<LevelConfiguration>
{ 
    [SerializeField] private int _levelCount;
    [SerializeField] private float _additionalScale;
    [SerializeField] private GameObject _beam;
    [SerializeField] private Ball _ball;

    [Header("Platforms")]
    [SerializeField] private SpawnPlatform _spawnPlatform;
    [SerializeField] private Platform[] _platform;
    [SerializeField] private FinishPlatform _finishPlatform;

    private float _startAndFinishAdditionalScale = 0.5f;
    public float BeamScaleY => _levelCount / 2.0f + _startAndFinishAdditionalScale + _additionalScale / 2.0f;

    public void OnSceneLoaded(LevelConfiguration level)
    {
        _levelCount = level.LivelCount;
        _platform = level.Platforms;
        Build();
    }

    private void Build()
    {
        GameObject beam = Instantiate(_beam.gameObject, transform);

        beam.transform.localScale = new Vector3(1.0f , BeamScaleY, 1.0f);
        Vector3 spawnPosition = beam.transform.position;
        spawnPosition.y += beam.transform.localScale.y - _additionalScale;

        _ball.transform.position = spawnPosition + _spawnPlatform.GetBallSpawnPoint().position;

        SpawnPlatform(_spawnPlatform, ref spawnPosition, beam.transform);
        for (int i = 0; i < _levelCount; ++i)
        {
            int randomIndex = Random.Range(0, _platform.Length);
            SpawnPlatform(_platform[randomIndex], ref spawnPosition, beam.transform);
        }
        SpawnPlatform(_finishPlatform, ref spawnPosition, beam.transform);
    }

    private void SpawnPlatform(Platform platform, ref Vector3 spawnPosition, Transform parent)
    {
        Platform current = Instantiate(platform, spawnPosition, Quaternion.identity, transform);
        current.transform.parent = parent;
        spawnPosition.y -= 1.0f;
    }
}
