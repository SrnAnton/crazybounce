using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof(Rigidbody))]
public class TowerRotator : MonoBehaviour
{
    [SerializeField] private float _rotateSpeed;

    private Rigidbody _rigidbody;

    public float _sensitivity = 5f;
    private Vector3 _mouseReference;
    private Vector3 _mouseOffset;
    private Vector3 _rotation = Vector3.zero;
    private bool _isRotating;


    private delegate void RotateTower();
    
    private RotateTower Rotate;

    private void Awake()
    {
        Rotate = TouchRotate;

#if UNITY_EDITOR || UNITY_STANDALONE_WIN
        Rotate = MouseRotete;
#endif
    }

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void FixedUpdate()
    {
        Rotate();
    }

    void TouchRotate()
    {
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Moved)
            {
                float torque = touch.deltaPosition.x * Time.fixedDeltaTime * _rotateSpeed;
                _rigidbody.AddTorque(Vector3.down * torque);
            }
        }
    }

    void MouseRotete()
    {
        if(Input.GetMouseButtonDown(0))
        {
            _isRotating = true;
            _mouseReference = Input.mousePosition;
        }

        if (Input.GetMouseButtonUp(0))
        {
            _isRotating = false;
        }

        if (_isRotating)
        {
            _mouseOffset = (Input.mousePosition - _mouseReference);
            _rotation.y = -(_mouseOffset.x + _mouseOffset.y) * _rotateSpeed * Time.fixedDeltaTime;
            _rigidbody.AddTorque(_rotation);
            _mouseReference = Input.mousePosition;
        }
    }
}
