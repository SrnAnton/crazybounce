﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IJunior.TypedScenes;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class MenuItem : MonoBehaviour
{
    [SerializeField] private Text _buttonText;
    private LevelConfiguration _level;

    public void SetLevel(LevelConfiguration level)
    {
        _level = level;
        _buttonText.text = level.Tag;
    }

    public void LoadLevel()
    {
        Game.Load(_level);
    }
}
