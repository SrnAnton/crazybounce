﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelMenu : MonoBehaviour
{
    [SerializeField] private GameObject _menuContent;
    [SerializeField] private MenuItem _item;
    [SerializeField] private LevelConfiguration[] _levels;

    private void Start()
    {
        foreach (var level in _levels)
        {
            MenuItem item = Instantiate(_item, _menuContent.transform);
            item.SetLevel(level);
        }
    }
}
