﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using IJunior.TypedScenes;
using UnityEngine.UI;

public class Score : MonoBehaviour, ISceneLoadHandler<LevelConfiguration>
{
    [SerializeField] private Ball _ball;
    [SerializeField] private Slider _timer;
    [SerializeField] private float _starTimerBonus;

    private float _time;
    private int _levelCount;
    private int _starsCount;
    private bool _isLaunched;

    private void Awake()
    {
        _starsCount = 0;
        _levelCount = 0;
        _timer.maxValue = 1.0f;
        _isLaunched = true;
    }

    private void Update()
    {
        if (_isLaunched)
        {
            _time += Time.deltaTime;
            _timer.value = _timer.maxValue - _time;
        }
    }

    private void OnEnable()
    {
        _ball.Finished += OnBallFinish;
        _ball.Died += OnBallDie;
        _ball.Awarded += OnBallAwarded;
    }

    private void OnDisable()
    {
        _ball.Died -= OnBallDie;
        _ball.Finished -= OnBallFinish;
        _ball.Awarded -= OnBallAwarded;
    }

    private void OnBallDie()
    {
        _isLaunched = false;
    }

    private void OnBallFinish()
    {
        _isLaunched = false;
    }

    private void OnBallAwarded()
    {
        _starsCount++;
        _time -= _starTimerBonus;
    }

    private void OnBallSpawned(Ball ball)
    {
        _ball = ball;
    }

    public void OnSceneLoaded(LevelConfiguration argument)
    {
        _levelCount = argument.LivelCount;
        _timer.maxValue = argument.LevelTime;
        _timer.value = argument.LevelTime;
        _starTimerBonus = argument.StarTimeBonus;
    }
}
