﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSegment : Segment
{
    [SerializeField] private int _countdown = 3;
  
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out BallJumper jumper))
        {
            jumper.Jump();
            _countdown--;
        }

        if (_countdown == 0)
        {
            Collapse();
        }
    }
}
