﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSegment : UsualSegment
{
    [SerializeField] private StarBonus _starBonus;

    protected override void Collapse()
    {
        if(_starBonus != null)
            _starBonus.Break();

        base.Collapse();
    }
}
