﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarBonus : Bonus
{
    public void Break()
    {
        Destroy(gameObject);
    }
}
