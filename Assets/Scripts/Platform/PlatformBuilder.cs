﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(Platform))]
public class PlatformBuilder : MonoBehaviour
{
    private enum SegmentType
    {
        Empty = 0,
        Standart = 1,
        Danger = 2,
        Star = 3
    }

    [Header("Segments Type")]
    [SerializeField] private Segment _emptySegment;
    [SerializeField] private Segment _standartSegment; 
    [SerializeField] private Segment _dangerSegment;
    [SerializeField] private Segment _bonusSegment;

    [Header("Platform Map")]
    [SerializeField] private SegmentType[] _map = new SegmentType[12];
    [SerializeField] private float _RotateAngle = 0.0f;
    
    [Header("Randomizer")]
    [SerializeField] private bool _isShuffled;
    [SerializeField] private bool _isRandomRotated;

    private readonly float _segmentRadius = 30.0f;

    private void Start()
    {
        Build();
        Rotate();   
    }
    private void Build()
    {
        float filledRadius = 0.0f;

        if (_isShuffled)
            Shuffle(_map);

        for (int i = 0; i < _map.Length; i++)
        {
            SpawnSegment(GetSegment(_map[i]), filledRadius);
            filledRadius += _segmentRadius;
        }
    }
    private void Shuffle<T>(T[] array)
    {
        for (int i = 0, k; i < array.Length; i++)
        {
            k = Random.Range(0, array.Length);
            (array[i], array[k]) = (array[k], array[i]);
        }
    }

    private void Rotate()
    {
        if (_isRandomRotated)
            _RotateAngle = Random.Range(0.0f, 360.0f);

        transform.rotation = Quaternion.Euler(0.0f, _RotateAngle, 0.0f);
    }

    private Segment GetSegment(SegmentType type)
    {    
        switch (type)
        {
            case SegmentType.Empty:
                return _emptySegment;

            case SegmentType.Standart:
                return _standartSegment;

            case SegmentType.Danger:
                return _dangerSegment;

            case SegmentType.Star:
                return _bonusSegment;

            default:
                throw new System.Exception("Bad value for SegmentType");
        }
    }

    private void SpawnSegment(Segment segment, float rotationAngle)
    {
        Quaternion rotation = Quaternion.Euler(-90.0f, transform.rotation.y, rotationAngle);
        Instantiate(segment, transform.position, rotation, transform);   
    }
}
