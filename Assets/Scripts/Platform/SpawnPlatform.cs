using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SpawnPlatform : Platform
{
    [SerializeField] private Transform _spawnPoint;
   
    public Transform GetBallSpawnPoint()
    {
        return _spawnPoint;
    }
}
