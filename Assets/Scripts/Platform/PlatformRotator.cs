﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlatformRotator : MonoBehaviour
{
    [SerializeField] private float _animationDuration;
    [SerializeField] private LoopType _loopType;
    [SerializeField] private RotateMode _rotateMode;

    private void Start()
    {
        transform.DORotate(new Vector3(0.0f, 360.0f, 0.0f), _animationDuration, _rotateMode).SetLoops(-1, _loopType);
    }
}
