﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IJunior.TypedScenes;


public class GameSceneSwitcher : MonoBehaviour, ISceneLoadHandler<LevelConfiguration>
{
    [SerializeField] private Ball _ball;
    private LevelConfiguration _level;

    private void OnEnable()
    {
        _ball.Finished += OnBallFinished;
        _ball.Died += OnBallDied;
    }

    private void OnDisable()
    {
        _ball.Finished -= OnBallFinished;
        _ball.Died -= OnBallDied;
    }

    private void OnBallDied()
    {
        Game.Load(_level);
    }

    private void OnBallFinished()
    {
        Present.Load();
    }

    public void OnSceneLoaded(LevelConfiguration level)
    {
        _level = level;
    }
}
