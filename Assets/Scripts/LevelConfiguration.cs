﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Config", menuName = "Config")]
public class LevelConfiguration : ScriptableObject
{
    [SerializeField] private Platform[] _platform;
    [SerializeField] private int _levelCount;
    [SerializeField] private int _levelTime;
    [SerializeField] private int _starTimeBonus;

    public string Tag => this.name;
    public Platform[] Platforms => _platform;
    public int LivelCount => _levelCount;
    public int LevelTime => _levelTime;
    public int StarTimeBonus => _starTimeBonus;

 
}
