using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Ball : MonoBehaviour
{
    public event UnityAction Died;
    public event UnityAction Finished;
    public event UnityAction Started;
    public event UnityAction Passed;
    public event UnityAction Awarded;
    public event UnityAction Falled;

    private bool IsAbove(Segment segment) => (transform.position.y - segment.transform.position.y) > 0;
    private bool _isAlive;
    private bool _isReact;

    private void Start()
    {
        _isAlive = true;
    }
  
    private void OnTriggerExit(Collider other)
    {
        if (other.TryGetComponent(out Segment segment))
        {
            other.GetComponentInParent<Platform>().Break();
            Passed?.Invoke();
        }
        else if (other.TryGetComponent(out StarBonus star))
        {
            star.Break();
            Awarded?.Invoke();
        }
    }
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.TryGetComponent(out UsualSegment usual))
        {
            if (IsAbove(usual))
                Falled?.Invoke();
        }
        else if (collision.gameObject.TryGetComponent(out DangerousSegment danger))
        {
            if(_isAlive)
                Died?.Invoke();
            _isAlive = false;
        }
        else if (collision.gameObject.TryGetComponent(out FinishSegment finish))
        {
            if (_isAlive)
                Finished?.Invoke();
            _isAlive = false;
        }
        else if (collision.gameObject.TryGetComponent(out StartSegment start))
        {
            Started?.Invoke();
        }
    }



}
