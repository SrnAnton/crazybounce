﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Ball))]
public class BallHarmonist : MonoBehaviour
{
    [SerializeField] private AudioSource[] _audioSources;
    private Ball _ball;

    private void Awake()
    {
        _ball = GetComponent<Ball>();
    }
    private void OnEnable()
    {
        _ball.Falled += OnFalled;
        _ball.Awarded += OnAwarded;
        _ball.Passed += OnPassed;
    }

    private void OnDisable()
    {
        _ball.Falled -= OnFalled;
        _ball.Awarded -= OnAwarded;
        _ball.Passed -= OnPassed;
    }

    private void OnAwarded()
    {
        _audioSources[0].Play();
    }

    private void OnFalled()
    {
        _audioSources[1].Play();
    }

    private void OnPassed()
    {
        _audioSources[2].Play();
    }

}


