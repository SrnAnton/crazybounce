using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(Ball))]
public class BallJumper : MonoBehaviour
{
    [SerializeField] private float _jumpForce;
    private float _lastposistion;
    private Rigidbody _rigidbody;
    private Ball _ball;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _ball = GetComponent<Ball>();
    }

    private void OnEnable()
    {
        _ball.Falled += OnFalled;
    }

    private void OnDisable()
    {
        _ball.Falled -= OnFalled;
    }

    private void OnFalled()
    {
        Jump();
    }

    public void Jump()
    {
        _rigidbody.velocity = Vector3.zero;
        _rigidbody.isKinematic = true;
        _rigidbody.isKinematic = false;

        _rigidbody.AddForce(Vector3.up * _jumpForce, ForceMode.Impulse);
    }

}
